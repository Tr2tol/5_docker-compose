# Используем базовый образ Python 3.8
FROM python:3.8-slim
# Устанавливаем netcat для проверки запущенности db хотя это лучше делать в самом питоне проверять доступна ли база
RUN apt-get update && apt-get install -y netcat
# Устанавливаем необходимые зависимости
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

# Устанавливаем переменные окружения
ENV FLASK_APP=src/app.py

# Создаем пользователя app
RUN useradd -ms /bin/bash app
USER app

# Копируем исходный код приложения
COPY . /app
# Устанавливаем рабочую директорию
WORKDIR /app

# Очищаем кэш пакетного менеджера и директорию /var/lib/apt/lists/
USER root
RUN rm -rf /var/lib/apt/lists/* && apt-get clean

# Возвращаемся к пользователю app
USER app
