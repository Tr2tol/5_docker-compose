#!/bin/bash
until nc -z "$DB_HOST" "$DB_PORT"; do
    echo "$(date) - waiting for postgres..."
    sleep 1
done
